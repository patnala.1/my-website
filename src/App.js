import logo from './logo.svg';
import './App.css';
import Board from './components/Board';

function App() {

  function refreshpage(){
    window.location.reload(false);
  }

  function shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  const array=[
    {id:1, name:'square', active:false},
    {id:2, name:'square', active:false},
    {id:3, name:'square', active:false},
    {id:4, name:'circle', active:false},
    {id:5, name:'circle', active:false},
    {id:6, name:'circle', active:false},
    {id:7, name:'triangle-top', active:false},
    {id:8, name:'triangle-top', active:true},
    {id:9, name:'triangle-top', active:false},
    {id:10, name:'square', active:false},
    {id:11, name:'square', active:false},
    {id:12, name:'square', active:false},
    {id:13, name:'square', active:false},
    {id:14, name:'square', active:false},
    {id:15, name:'square', active:false},
    {id:16, name:'square', active:false},
    {id:17, name:'square', active:false},
    {id:18, name:'square', active:false},
    {id:19, name:'circle', active:false},
    {id:20, name:'circle', active:false},
    {id:21, name:'circle', active:false},
    {id:22, name:'circle', active:false},
    {id:23, name:'circle', active:false},
    {id:24, name:'circle', active:false},
    {id:25, name:'circle', active:false},
    {id:26, name:'circle', active:false},
    {id:27, name:'circle', active:false},
    {id:28, name:'triangle-left', active:false},
    {id:29, name:'triangle-left', active:false},
    {id:30, name:'triangle-left', active:false},
    {id:31, name:'triangle-right', active:false},
    {id:32, name:'triangle-right', active:false},
    {id:33, name:'triangle-right', active:false},
    {id:34, name:'triangle-bottom', active:false},
    {id:35, name:'triangle-bottom', active:false},
    {id:36, name:'triangle-bottom', active:false},
    

  ]

  const array1= shuffleArray(array)
  return (
    <div className="React">
      <h3>Assignment - venkatesh</h3>
    <div className="App">
      <Board array1={array1}/>
      <button onClick={()=>refreshpage()} className='button' > Refresh </button>

      
    </div>
    <div>
        <p className="heading">Interactions-</p>
        <ul>
          <li>On clicking a triangle you will move to end of row or column depending on the direction of the edge of triangle</li>
          <li>on clicking on a square it will move one box reverse to the previous state from where it came from</li>
          <li>on clicking on a circle it move one venky syam perpendicularly clockwise depending on the direction.</li>
          <li> the variable is : {process.env.REACT_APP_VAR_TOKEN}</li>
        </ul>
      </div>
    </div>
  );
}

export default App;

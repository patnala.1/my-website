import React, { Component, useState } from "react";
import "./Board.css";
export default function Board(props) {
  const [localarray, setlocalarray] = useState([...props.array1]);
  const [prev, setprev] = useState(0);
  const [prevstate, setprevstate] = useState("triangle-top");
  const [prevdirection, setprevdirection] = useState("top");

  const handleClick = ({ element }) => {
    let arrayfunc = [...localarray];
    let pre, prestate, predirection;
    if (element.active === true) {
      for (var i = 0; i < arrayfunc.length; i++) {
        if (arrayfunc[i].id === element.id) {
          pre = i;
          prestate = element.name;
          arrayfunc[i].active = false;
          let x, j;
          switch (element.name) {
            case "triangle-top":
              x = parseInt(i / 6);
              j = i - 6 * x;

              predirection = "top";
              break;
            case "triangle-bottom":
              x = parseInt(i / 6);
              j = i + 6 * (5 - x);

              predirection = "bottom";
              break;
            case "triangle-left":
              x = i % 6;
              j = i - x;

              predirection = "left";
              break;
            case "triangle-right":
              x = i % 6;
              j = i + (5 - x);

              predirection = "right";
              break;
            case "square":
              switch (prevdirection) {
                case "top":
                  j = i + 6;

                  predirection = "bottom";
                  break;
                case "bottom":
                  j = i - 6;

                  predirection = "top";
                  break;
                case "left":
                  j = i + 1;

                  predirection = "right";
                  break;
                case "right":
                  j = i - 1;

                  predirection = "left";
                  break;
              }
              break;
            case "circle":
              switch (prevdirection) {
                case "top":
                  if (i % 6 === 5) {
                    j = -1;
                  } else {
                    j = i + 1;
                    predirection = "right";
                  }

                  break;
                case "bottom":
                  if (i % 6 === 0) {
                    j = -1;
                  } else {
                    j = i - 1;
                    predirection = "left";
                  }

                  break;
                case "left":
                  if (i <= 5) {
                    j = -1;
                  } else {
                    j = i - 6;
                    predirection = "top";
                  }

                  break;
                case "right":
                  if (i >= 30) {
                    j = -1;
                  } else {
                    j = i + 6;
                    predirection = "bottom";
                  }

                  break;
              }
              break;
          }
          if (j < 0 || j > 35) {
            alert("You got to an undefined loop...refresh to start new");
            window.location.reload(false);
          } else if (j === i) {
            alert("Sorry...loop ended..refresh to get new one");
            window.location.reload(false);
          } else {
            arrayfunc[j].active = true;
          }
        }
      }
    } else {
      alert("You can click only on active cell");
    }
    setprev(pre);
    setprevstate(prestate);
    setprevdirection(predirection);
    setlocalarray(arrayfunc);
    console.log(arrayfunc);
    console.log(pre);
    console.log(prestate);
  };
  return (
    <div className="board">
      {localarray.map((element) => (
        <div
          className={element.active ? "box" : "box1"}
          key={element.id}
          onClick={() => handleClick({ element })}
        >
          <div className={element.name}></div>
        </div>
      ))}
    </div>
  );
}
